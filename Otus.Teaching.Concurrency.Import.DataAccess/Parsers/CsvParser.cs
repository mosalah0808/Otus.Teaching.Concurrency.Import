﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;
using Microsoft.VisualBasic.FileIO;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
       

        public List<Customer> Parse(string filePath)
        {
            using var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var customers = CsvSerializer.DeserializeFromStream<List<Customer>>(fileStream);
            return customers;


        }
        public List<string> ReadFile(string filePath)
        {
            var csv = new List<string>();

            using (var reader = new StreamReader(filePath))
            {
                string? line = "";
                while ((line = reader.ReadLine()) != null)
                    csv.Add(line);
            }
            return csv;
        }
    }
}
