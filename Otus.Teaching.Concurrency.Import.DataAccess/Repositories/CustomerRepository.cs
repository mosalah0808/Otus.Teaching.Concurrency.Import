using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private CustomerContext _context;
        private DbSet<Customer> _dbSet;

        public CustomerRepository(CustomerContext context)
        {
        }
        public async Task<bool> AddCustomer(Customer customer)
        {
            try
            {
                using var _dataContext = new CustomerContext();
                await _dataContext.Set<Customer>().AddAsync(customer);
                await _dataContext.SaveChangesAsync();
                await _dataContext.DisposeAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public void Clear()
        {
            _dbSet.RemoveRange(_dbSet);
            SaveChange();
          
        }

        public  void SaveChange()
        {
             _context.SaveChanges();
        }
       
       
    }
}