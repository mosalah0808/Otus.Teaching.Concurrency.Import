using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;



namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public static class Extensions
    {
        public static IEnumerable<List<Customer>> Partition(this List<Customer> values, int number)
        {
            var batch = new List<Customer>(number);

            foreach (var item in values)
            {
                batch.Add(item);

                if (batch.Count == number)
                {
                    yield return batch;
                    batch = new List<Customer>(number);
                }
            }

            if (batch.Count > 0)
            {
                yield return batch;
            }
        }
    }
    public class DataLoader : IDataLoader
    {
        private List<Customer> _customers;
        private CustomerRepository _repository;
        public event Action<string> DisplayMessage;
        private int _numberOfThreads;
        private int _number;
        
     
        public DataLoader(List<Customer> customers,  int numberOfThreads, int number)
        {
            _customers = customers;
            _number = number;
            _repository = new CustomerRepository(new CustomerContext());
            _numberOfThreads = numberOfThreads;
                       
            Console.WriteLine($"Starting...");
        }
        public void LoadData()
        {
            var stopWatch = new Stopwatch();
            Console.WriteLine($"Start load without thread...");
            stopWatch.Start();
            ParseNotThread();
            stopWatch.Stop();
            Console.WriteLine($"Load without thread finish after {stopWatch.Elapsed} second.");
            Console.WriteLine();

            Console.WriteLine($"Start load with thread...");
            stopWatch = new Stopwatch();
            stopWatch.Start();
            ParseWithThread(_numberOfThreads);
            stopWatch.Stop();
            Console.WriteLine($"Load with thread finish after {stopWatch.Elapsed} second.");
            Console.WriteLine();

            Console.WriteLine($"Start load with threadpool...");
            stopWatch = new Stopwatch();
            stopWatch.Start();
            ParseWithThreadpool(_numberOfThreads);
            stopWatch.Stop();
            Console.WriteLine($"Load with threadpool finish after {stopWatch.Elapsed} second.");
            Console.WriteLine();

        }

        private void ParseWithThread(int numberThreads)
        {
           
            _repository.Clear();
          
            Barrier barrier = new Barrier(numberThreads + 1);
            var parts = Extensions.Partition(_customers, _number);
            
            foreach (var part in parts)
            {
                Thread thread = new Thread(() =>
                {
                    LoadListInRepository(part);
                    barrier.SignalAndWait();
                });
                thread.Start();
               
            }
            barrier.SignalAndWait();
          


        }
        private void ParseWithThreadpool(int numberThreads)
        {
            _repository.Clear();
           
            var countdownEvent = new CountdownEvent(_numberOfThreads);
            var parts = Extensions.Partition(_customers, _number);

            foreach (var part in parts)
            {
                var action = new Action(() =>
                {
                    LoadListInRepository(part);
                    countdownEvent.Signal();

                });
                ThreadPool.QueueUserWorkItem(start => action());
                
            }
            countdownEvent.Wait();
            countdownEvent.Dispose();          
        }
           
        private void ParseNotThread()
        {
            _repository.Clear();
            LoadListInRepository(_customers);
            
        }


        private void LoadListInRepository(List<Customer> customers)
        {
            _repository = new CustomerRepository(new CustomerContext());
            foreach (var customer in customers)
                _repository.AddCustomer(customer).GetAwaiter().GetResult(); ;
        
        }
 }


}